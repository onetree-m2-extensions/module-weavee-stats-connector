<?php

/**
 * @author     Juan Pablo Müller <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */

namespace Onetree\WeaveeDashboard\Block;

use Laminas\Http\Client;
use Laminas\Http\Headers;
use Laminas\Http\Request;
use Laminas\Json\Json;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\ConfigurationMismatchException;

class DashboardIframe extends \Magento\Backend\Block\Template
{
    protected const LOGIN_CLIENT_HEADERS = [
        "Content-type: application/json"
    ];
    protected const CONFIG_ENVS = [
        'production' => [
            'get_jwt_token' => 'https://dev-weavee.azurewebsites.net/api/clients/{{client_id}}/admin/token',
            'dashboard' => 'https://weavee-dev.azureedge.net/adobe?{{key_name}}={{jwt_key}}',
            'headers' => [
                'x-functions-key: s4riURD5WYaDwUURiRcd-43sG52RK12lf5iMhdUAWzAAAzFuve1gyA=='
            ]
        ],
        'testing' => [
            'get_jwt_token' => 'https://dev-weavee.azurewebsites.net/api/clients/{{client_id}}/admin/token',
            'dashboard' => 'https://weavee-dev.azureedge.net/adobe?{{key_name}}={{jwt_key}}',
            'headers' => [
                'x-functions-key: s4riURD5WYaDwUURiRcd-43sG52RK12lf5iMhdUAWzAAAzFuve1gyA=='
            ]
        ],
    ];
    protected const KEY_URL_NAME = 'token';
    protected const APIKEY_PARAM_NAME = 'apiKey';

    protected const KEY_PATH = 'weavee_dashboard_config/general_settings/api_key';
    protected const CLIENT_PATH = 'weavee_dashboard_config/general_settings/client_id';
    protected const DASHBOARD_ENABLED_PATH = 'weavee_dashboard_config/general_settings/enable_dashboard';
    protected const APP_ENV_PATH = 'weavee_dashboard_config/general_settings/app_env';
    /**
     * @var string
     */
    protected $_template = 'Onetree_WeaveeDashboard::dashboard.phtml';

    /**
     * @var Client
     */
    private Client $httpClient;
    /**
     * @var string|null
     */
    private string $selectedEnv;

    /**
     * Class constructor
     *
     * @param Context $context
     * @param Client $client
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        Client                                  $client,
    ) {
        parent::__construct($context);
        $this->httpClient = $client;
    }

    /**
     * Returns dashboard url
     *
     * @return array|string|string[]
     * @throws AuthenticationException
     * @throws ConfigurationMismatchException
     */
    public function getDashboardUrlWithKey()
    {
        $isDashboardEnabled = $this->_scopeConfig->getValue(self::DASHBOARD_ENABLED_PATH);

        if (!$isDashboardEnabled) {
            throw new ConfigurationMismatchException(__('Dashboard is disabled'));
        }

        $this->selectedEnv = $this->_scopeConfig->getValue(self::APP_ENV_PATH);
        if (!$this->selectedEnv) {
            throw new ConfigurationMismatchException(__('Environment type is not selected'));
        }

        $token = $this->getClientToken();

        return str_replace(
            ['{{key_name}}', '{{jwt_key}}'],
            [self::KEY_URL_NAME, $token],
            self::CONFIG_ENVS[$this->selectedEnv]['dashboard']
        );
    }

    /**
     * Get dashboard client token
     *
     * @return mixed
     * @throws AuthenticationException
     * @throws ConfigurationMismatchException
     */

    private function getClientToken()
    {
        $key = $this->_scopeConfig->getValue(self::KEY_PATH);
        $clientId = $this->_scopeConfig->getValue(self::CLIENT_PATH);

        if (!$key || !$clientId) {
            throw new ConfigurationMismatchException(__('Configuration is not completed'));
        }

        $headers = new Headers();

        foreach (array_merge(self::LOGIN_CLIENT_HEADERS, self::CONFIG_ENVS[$this->selectedEnv]['headers']) as $header) {
            $headers->addHeaderLine($header);
        }

        $this->httpClient
            ->setMethod(Request::METHOD_POST)
            ->setUri(str_replace('{{client_id}}', $clientId, self::CONFIG_ENVS[$this->selectedEnv]['get_jwt_token']))
            ->setRawBody(Json::encode([self::APIKEY_PARAM_NAME => $key]))
            ->setHeaders($headers);

        $response = $this->httpClient->send();

        if (!$response->isSuccess()) {
            throw new AuthenticationException(__('Login failed. Check credentials or try later'));
        }

        return Json::decode($response->getBody())->data;
    }
}
