<?php

/**
 * @author     Juan Pablo Müller <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */

namespace Onetree\WeaveeDashboard\Controller\Adminhtml\Dashboard;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var bool|\Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory = false;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Execute controller
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Onetree_WeaveeDashboard::weavee_dashboard');
        $resultPage->getConfig()->getTitle()->prepend(__('Weavee'));
        return $resultPage;
    }
}
