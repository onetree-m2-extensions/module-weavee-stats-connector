<?php

/**
 * @author     Juan Pablo Müller <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */

namespace Onetree\WeaveeDashboard\Model\Config\Source;

class ListEnv implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Return dropdown options
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'production', 'label' => __('Production')],
            ['value' => 'testing', 'label' => __('Testing')],
        ];
    }
}
