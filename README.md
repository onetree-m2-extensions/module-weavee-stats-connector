# Onetree_WeaveeDashboard module

This module allows to show a third-party client dashboard from Weavee, see https://www.weavee.io/.


### Steps
1. Go to your Adobe Commerce admin panel
2. Set your credentials in Onetree (Sidebar icon) > Weavee > Configuration
3. See your dashboard in Onetree (Sidebar icon) > Weavee > Dashboard

### How to install

Via composer adding the repo:

<sup><sub><b>Note:</b> Needs credentials</sub></sup>
```
"onetree": {
    "type": "composer",
    "url": "https://gitlab.com/api/v4/group/77370416/-/packages/composer/packages.json"
}
```
And then use composer require:
```
# composer require onetree/module-weavee-stats-connector
```
