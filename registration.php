<?php

/**
 * @author     Juan Pablo Müller <juanpablo.muller@onetree.com>
 * @copyright  Copyright (©) 2023 Onetree
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Onetree_WeaveeDashboard', __DIR__);
